msitools (0.101+repack-1) unstable; urgency=medium

  * Avoid generating an empty wixl.h during the build.
  * Use the packaged bats instead of the bundled version.
  * Skip the tests on big-endian architectures until we determine what’s
    broken.

 -- Stephen Kitt <skitt@debian.org>  Mon, 08 Feb 2021 19:17:33 +0100

msitools (0.101-1) unstable; urgency=medium

  * New upstream release, building using Meson and dropping the uuid
    build-dependency.

 -- Stephen Kitt <skitt@debian.org>  Thu, 04 Feb 2021 13:50:25 +0100

msitools (0.100-2) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Set upstream metadata fields: Bug-Database, Bug-Submit (from
    ./configure), Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Stephen Kitt ]
  * Switch to debhelper compatibility level 13.
  * Standards-Version 4.5.1, no further change required.
  * Bump debian/watch version, fix it to avoid scanning the parent
    directory.

 -- Stephen Kitt <skitt@debian.org>  Wed, 06 Jan 2021 09:05:40 +0100

msitools (0.100-1) unstable; urgency=medium

  * New upstream release, merging big-endian.patch.
  * Standards-Version 4.4.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 16 Nov 2019 14:30:21 +0100

msitools (0.99-2) unstable; urgency=medium

  * Convert little-endian values on big-endian platforms.

 -- Stephen Kitt <skitt@debian.org>  Wed, 17 Jul 2019 21:10:10 +0200

msitools (0.99-1) unstable; urgency=medium

  * New upstream release.
  * Switch to debhelper compatibility level 10 (newer levels break because
    of --runstatedir and libtool).
  * Switch to secure debian/watch URL.
  * Specify the build-dependency package in libmsi0.symbols.
  * Standards-Version 4.4.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 16 Jul 2019 18:48:43 +0200

msitools (0.98-1) unstable; urgency=medium

  * New upstream release.
  * Migrate to Salsa.
  * The debug package migration is complete, drop the dh_strip override.
  * Set “Rules-Requires-Root: no”.
  * Standards-Version 4.2.1, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 06 Oct 2018 20:23:42 +0200

msitools (0.97-1) unstable; urgency=medium

  * New upstream release, merging summary-dates.patch, double-frees.patch
    and invalid-free.patch.

 -- Stephen Kitt <skitt@debian.org>  Wed, 02 Aug 2017 22:40:40 +0200

msitools (0.96-5) unstable; urgency=medium

  * Fix an invalid free on string properties. Closes: #869082. Thanks to
    Jakub Wilk for finding the crash!
  * Following the package split, wixl-data needs to break/replace wixl...
    Closes: #870224. Thanks to Andreas Beckmann for his tireless tests.

 -- Stephen Kitt <skitt@debian.org>  Mon, 31 Jul 2017 09:02:04 +0200

msitools (0.96-4) unstable; urgency=medium

  * Fix a double-free on string properties. Closes: #868795. Thanks to
    Jakub Wilk for finding the crash!

 -- Stephen Kitt <skitt@debian.org>  Wed, 19 Jul 2017 11:05:57 +0200

msitools (0.96-3) unstable; urgency=medium

  * As per the GObject-Introspection policy, the typelib package should be
    gir1.2-libmsi-1.0, not gir1.2-libmsi0; rename the package accordingly.
    The -dev package also needs a dependency on the typelib package.
  * Clean up all the generated files, to allow building twice in a row
    (or more).
  * Move shared data from wixl to a new wixl-data package.
  * Update debian/copyright.
  * Standards-Version 4.0.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Wed, 12 Jul 2017 23:13:22 +0200

msitools (0.96-2) unstable; urgency=medium

  * Add patch to correctly parse summary information dates. Closes:
    #793844.

 -- Stephen Kitt <skitt@debian.org>  Mon, 21 Nov 2016 21:05:00 +0100

msitools (0.96-1) unstable; urgency=medium

  * New upstream release.
  * Migrate to dbgsym debug packages.
  * Switch to https: VCS URIs (see #810378).
  * Clean up debian/control using cme.
  * Standards-Version 3.9.8, no change required.
  * Enable all hardening options.
  * Install NEWS as the upstream changelog.

 -- Stephen Kitt <skitt@debian.org>  Wed, 16 Nov 2016 18:44:00 +0100

msitools (0.95-1) unstable; urgency=medium

  * New upstream release.
  * Correct gir1.2-libmsi0 Multi-Arch declaration.
  * Make gir1.2-libmsi0 depend on libmsi0. Closes: #795249.
  * Update debian/copyright.

 -- Stephen Kitt <skitt@debian.org>  Sun, 06 Dec 2015 17:27:01 +0100

msitools (0.94-1) unstable; urgency=medium

  * New upstream release.
  * Drop msi-table.patch, merged upstream.

 -- Stephen Kitt <skitt@debian.org>  Fri, 24 Jul 2015 22:46:26 +0200

msitools (0.93-1) unstable; urgency=low

  * Initial release. Closes: #757007.

 -- Stephen Kitt <skitt@debian.org>  Tue, 30 Dec 2014 22:15:40 +0100
